This program will render geotechnical site investigation data in a
graphical form.

Inputs are provided as .xlsx files.  See "data" directory for a
sample.

We will have boring sites, and for each boring site, we will have:

 - Boring ID
 - Ground elevation (in feet)
 - "Easting"
 - "Northing"
 - GPS Coordinates
 - Plunge, in degrees
 - Orientation
 - Total depth
 - Date started
 - Date completed
 - Monitoring well
 - Water level (in feet)
 - Notes

For individual sites, we will have the following:

 - Lithology
 - Lithology group
 - Top depth (in feet)
 - Bottom depth (in feet)
